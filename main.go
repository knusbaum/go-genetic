package main

import (
	"fmt"
	"math"
	"sort"
	"math/rand"
	"github.com/veandco/go-sdl2/sdl"
)

const WIDTH = 1024
const HEIGHT = 768
const START_HEALTH = 2000
const RED_HEALTH = 10
const BLUE_HEALTH = 10
const GENE_POOL_SIZE = 100
const MUTATION_FACTOR = 3

const FOOD_COUNT = 20
const DANGER_COUNT = 2

const RED_BENEFIT = 10
const BLUE_BENEFIT = 1

const RED_START = true
const BLUE_START = false


type Sim struct {
	window *sdl.Window
	surface *sdl.Surface
	actor *Actor
	food []*Food
	danger []*Danger
	actors []*Actor
	redbad bool
	bluebad bool
}

type Food struct {
	rect sdl.Rect
}

type Danger struct {
	rect sdl.Rect
}

type Actor struct {
	foodxmult float64
	foodymult float64
	foodexp float64

	dangerxmult float64
	dangerymult float64
	dangerexp float64

	rect sdl.Rect
	health int
	rank int
}

func (s *Sim)spawnEntities() {
	s.food = make([]*Food, FOOD_COUNT)
	for f := range s.food {
		s.food[f] = &Food{randrect()}
	}
	s.danger = make([]*Danger, DANGER_COUNT)
	for d := range s.danger {
		s.danger[d] = &Danger{randrect()}
	}
}

func (a *Actor)move(sim *Sim) {
	dirx := 0.0
	diry := 0.0
	for food := range sim.food {
		disty := float64(a.rect.Y - sim.food[food].rect.Y)
		distx := float64(a.rect.X - sim.food[food].rect.X)
		dist := math.Abs(disty) + math.Abs(distx)

		if disty != 0 {
			//diry = (diry + (disty * a.foodymult)) * (1/math.Pow(dist, a.foodyexp))
			diry += (disty * a.foodymult) * (1/math.Pow(dist, a.foodexp))
		}
		if distx != 0 {
			dirx += (distx * a.foodxmult) * (1/math.Pow(dist, a.foodexp))
		}
	}

	for danger := range sim.danger {
		disty := float64(a.rect.Y - sim.danger[danger].rect.Y)
		distx := float64(a.rect.X - sim.danger[danger].rect.X)
		dist := math.Abs(disty) + math.Abs(distx)

		if disty != 0 {
			//diry = (diry + (disty * a.foodymult)) * (1/math.Pow(dist, a.foodyexp))
			diry += (disty * a.dangerymult) * (1/math.Pow(dist, a.dangerexp))
		}
		if distx != 0 {
			dirx += (distx * a.dangerxmult) * (1/math.Pow(dist, a.dangerexp))
		}
	}


	if dirx > 0 {
		a.rect.X += 1
	} else if dirx < 0 {
		a.rect.X -= 1
	}

	if diry > 0 {
			a.rect.Y += 1
	} else if diry < 0 {
		a.rect.Y -= 1
	}
}

func randrect() sdl.Rect {
	return sdl.Rect{rand.Int31n(WIDTH), rand.Int31n(HEIGHT), 10, 10}
}

func newActor(a *Actor) *Actor {
	return &Actor{
		a.foodxmult + ((rand.Float64() - 0.5) * MUTATION_FACTOR),
		a.foodymult + ((rand.Float64() - 0.5) * MUTATION_FACTOR),
		a.foodexp + ((rand.Float64() - 0.5) * MUTATION_FACTOR),
		a.dangerxmult + ((rand.Float64() - 0.5) * MUTATION_FACTOR),
		a.dangerymult + ((rand.Float64() - 0.5) * MUTATION_FACTOR),
		a.dangerexp + ((rand.Float64() - 0.5) * MUTATION_FACTOR),
		randrect(),
		START_HEALTH,
		0,
	}
}

func update(sim *Sim) {
	sim.actor.health -= 1
	if sim.actor.health <= 0 {
		//sim.actor.health = START_HEALTH
		//sim.actor.rect = randrect()
		//if len(sim.actors) == 0 || sim.actor.rank > 0 {
			sim.actors = append(sim.actors, sim.actor)
		    rand.Shuffle(len(sim.actors), func(i, j int) { sim.actors[i], sim.actors[j] = sim.actors[j], sim.actors[i] })
		    sort.Slice(sim.actors,
				func(i, j int) bool {
					return sim.actors[i].rank > sim.actors[j].rank
				})
			if len(sim.actors) > GENE_POOL_SIZE {
				sim.actors = sim.actors[:GENE_POOL_SIZE]
			}
		//}

		copyActor := sim.actors[rand.Intn(len(sim.actors))]
		copyActor.rank = int(float64(copyActor.rank) * 0.8)
		sim.actor = newActor(copyActor)
		//fmt.Printf("Actor %#v\n", sim.actor)
		avgFitness := 0.0
		for actor := range sim.actors {
			avgFitness += float64(sim.actors[actor].rank)
			//fmt.Printf("%d, ", sim.actors[actor].rank)
		}
		//fmt.Println("")
		avgFitness = avgFitness / float64(len(sim.actors))
		//fmt.Printf("AVG FITNESS: %f\n", avgFitness)
		fmt.Printf("%f\n", avgFitness)
		sim.spawnEntities()
	}

	sim.actor.move(sim)

	for food := range sim.food {
		disty := float64(sim.actor.rect.Y - sim.food[food].rect.Y)
		distx := float64(sim.actor.rect.X - sim.food[food].rect.X)
		dist := math.Abs(disty) + math.Abs(distx)
		if dist < 10 {
			if sim.bluebad {
				sim.actor.health = 0
			} else {
				sim.actor.health += BLUE_HEALTH
				sim.actor.rank += BLUE_BENEFIT
			}
			sim.food[food].rect = randrect()
		}
	}

	for danger := range sim.danger {
		disty := float64(sim.actor.rect.Y - sim.danger[danger].rect.Y)
		distx := float64(sim.actor.rect.X - sim.danger[danger].rect.X)
		dist := math.Abs(disty) + math.Abs(distx)
		if dist < 20 {
			if sim.redbad {
				sim.actor.health = 0
				//sim.actor.rank -= 10
			} else {
				sim.actor.health += RED_HEALTH
				sim.actor.rank += RED_BENEFIT
			}
			sim.danger[danger].rect = randrect()
		}
	}

	//fmt.Printf("Actor %#v health: %d\n", sim.actor, sim.actor.health)
}

func render(sim *Sim) {
	sim.surface.FillRect(nil, 0)
	sim.surface.FillRect(&sim.actor.rect, 0xff00ff00)
	for food := range sim.food {
		sim.surface.FillRect(&sim.food[food].rect, 0xff0000ff)
	}
	for danger := range sim.danger {
		sim.surface.FillRect(&sim.danger[danger].rect, 0xffff0000)
	}
	sim.window.UpdateSurface()
}

func main() {
	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		panic(err)
	}
	defer sdl.Quit()

	window, err := sdl.CreateWindow("test", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		WIDTH, HEIGHT, sdl.WINDOW_SHOWN)
	if err != nil {
		panic(err)
	}
	defer window.Destroy()

	surface, err := window.GetSurface()
	if err != nil {
		panic(err)
	}

	sim := &Sim{
		window,
		surface,
		&Actor{
			0,
			0,
			0,
			0,
			0,
			0,
			sdl.Rect{300, 200, 10, 10},
			START_HEALTH,
			0,
		},
		[]*Food{},
		[]*Danger{},
		make([]*Actor, 0),
		RED_START,
		BLUE_START,
	}
	sim.spawnEntities()
	running := true
	shouldrender := true
	for running {
		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch event.(type) {
			case *sdl.QuitEvent:
				println("Quit")
				running = false
				break
			case *sdl.KeyDownEvent:
				keydown := event.(*sdl.KeyDownEvent)
//				if keydown.Keysym.Unicode == 'a' {
//					println("Don't Render.")
				//				}
				keyname := sdl.GetKeyName(sdl.GetKeyFromScancode(keydown.Keysym.Scancode))
				//fmt.Printf("Got Key: %s\n", keyname)
				if keyname == "A" {
					shouldrender = !shouldrender
				} else if keyname == "O" {
					sim.redbad = !sim.redbad
					if sim.redbad {
						fmt.Printf("RED poison\n")
					} else {
						fmt.Printf("RED food\n")
					}
				} else if keyname == "E" {
					sim.bluebad = !sim.bluebad
					if sim.bluebad {
						fmt.Printf("BLUE poison\n")
					} else {
						fmt.Printf("BLUE food\n")
					}
				}
			}
		}
		update(sim)
		if shouldrender {
			render(sim)
			sdl.Delay(1)
		}
	}
}
